$(document).ready(function() {

    // Add browser body class

    var is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
    var is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
    var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
    var is_safari = navigator.userAgent.indexOf("Safari") > -1;
    var is_opera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
    if ((is_chrome)&&(is_safari)) {is_safari=false;}
    if ((is_chrome)&&(is_opera)) {is_chrome=false;}


    if (is_chrome) {
        $('body').addClass('chrome');
    }

    if (is_firefox) {
        $('body').addClass('firefox');
    }

    if (is_explorer) {
        $('body').addClass('internetExplorer');
    }

    // Initialize Slides

    var slide = $('.slide');
    var slides = $('.slides');
    var i;
    var windowWidth = $(window).width();
    var headerHeight = $('header').outerHeight();
    var windowHeight = $(window).height() - headerHeight;
    var headerPercent = (headerHeight) / $(window).height();


    // Init ScrollMagic
    var controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: headerPercent
        }
    });

    // $(window).on('resize', function() {
    //     var headerPercent = .5;
    //
    // });

    // start scrollFunc
    var scrollFunc = function() {

        $(slide).last().siblings().each(function(i) {
            i++;

            var firstNineSlides = new ScrollMagic.Scene({
                triggerElement: this
            })
            .setPin(this)
            // .removePin(true)
            //.addIndicators()
            .reverse(true)
            .addTo(controller);

            $(window).on('scroll', function() {
                var curPos = $(window).scrollTop();
                var slideTop = $(slides).offset().top + $(slides).height();

                if (curPos > slideTop) {
                    firstNineSlides.destroy();
                } else {
                    firstNineSlides.addTo(controller);
                }
            });

            $(window).on('resize', function() {
                firstNineSlides.destroy();
            });

        });

        $(slide).last().each(function(i) {

            new ScrollMagic.Scene({
                triggerElement: this
            })
            .setPin(this)
            //.addIndicators()
            .reverse(true)
            // .removePin(true)
            .on("enter leave", removeFixed)
            .duration($(this).height())
            .addTo(controller);

        });

        function removeFixed (e) {

            if (e.type == "enter") {
                // $(slide).last().siblings().hide();
            } else {
                $(slide).css('position', 'relative');
            }
        }

    } // end scrollFunc


    // If windowWidth >= tablet
    if (windowWidth > 1024) {

        $('.section').css({
            'height': windowHeight
        });

        $('.section').first().css({
            'paddingTop': headerHeight,
            'height': windowHeight
        });

        $(slide).css({
            'height': windowHeight
        });

        $('.fullscreen-container').css({
            'height': windowHeight
        });

        $('.grey').each(function() {

            var greyShadow = new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 80
            })
            .setClassToggle("header", "greyShadow")
            // .on('enter', changeZindex)
            //.addIndicators()
            .addTo(controller);

        });

        $('.gold').each(function() {

            var goldShadow = new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 80
            })
            .setClassToggle("header", "goldShadow")
            //.addIndicators()
            .addTo(controller);

            $(window).on('resize', function() {
                goldShadow.destroy();
            });

        });

        $('.black').each(function() {

            var blackShadow =  new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 20
            })
            .setClassToggle("header", "blackShadow")
            //.addIndicators()
            .addTo(controller);

            $(window).on('resize', function() {
                blackShadow.destroy();
            });

        });

        $('.white').each(function() {

            var whiteShadow = new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 20
            })
            .setClassToggle("header", "whiteShadow")
            //.addIndicators()
            .addTo(controller);

            $(window).on('resize', function() {
                whiteShadow.destroy();
            });

        });

        $('.home-gold').each(function() {

            var homeShadow = new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight()
            })
            .setClassToggle("header", "goldShadow")
            //.addIndicators()
            .addTo(controller);

            $(window).on('resize', function() {
                homeShadow.destroy();
            });

        });

        $(window).on('scroll', function() {
            var curPos = $(window).scrollTop();

            if(curPos >= $('#contact').offset().top - headerHeight - windowHeight) {
                $(slide).last().siblings().css({'opacity': 0});
                $(slide).css('position', 'relative');
            }
        });

        $(window).on('resize', function() {
            $('header').removeClass('greyShadow');
            $('header').removeClass('goldShadow');
            $('header').removeClass('blackShadow');
            $('header').removeClass('whiteShadow');
            $('header').removeClass('homeShadow');
        });

        scrollFunc();

    } else {

        $(slide).css({
            'height': windowHeight
        });

        $('#about-us .container').removeClass('fullscreen-container');

        $('#home .container-large').removeClass('fullscreen-container');


        $('.fullscreen-container').css({
            'height': windowHeight
        });

        $('.grey').each(function() {

            new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 120
            })
            .setClassToggle("header", "greyShadow")
            //.addIndicators()
            .addTo(controller);

        });

        $('.gold').each(function() {

            new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 80
            })
            .setClassToggle("header", "goldShadow")
            //.addIndicators()
            .addTo(controller);

        });

        $('.black').each(function() {

            new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 80
            })
            .setClassToggle("header", "blackShadow")
            //.addIndicators()
            .addTo(controller);

        });

        $('.white').each(function() {

            new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight() - 80
            })
            .setClassToggle("header", "whiteShadow")
            //.addIndicators()
            .addTo(controller);

        });

        $('.home-gold').each(function() {

            new ScrollMagic.Scene({
                triggerElement: this,
                duration: $(this).outerHeight()
            })
            .setClassToggle("header", "goldShadow")
            //.addIndicators()
            .addTo(controller);

        });

        $('#mobile-menu-trigger').on('click', function(){
            $('.mobile-navigation').toggle();
            $(this).toggleClass('opened');
        });

        $('.mobile-navigation a').on('click', function() {
            $('.mobile-navigation').hide();
            $('#mobile-menu-trigger').removeClass('opened');
        });

        $('.mobile-navigation').css({
            'top': headerHeight
        });

    }


    $(window).on('scroll', function() {
        curPos = $(window).scrollTop();

        if(curPos >= $('#about-us').offset().top - headerHeight) {
            $('#about-us').css({'zIndex': 1});
        } else {
            $('#about-us').css({'zIndex': 1000});
        }

        if(curPos >= $('#investment-principles').offset().top - headerHeight) {
            $('#investment-principles').css({'zIndex': 1});
        } else {
            $('#investment-principles').css({'zIndex': 1001});
        }

        // if(curPos >= $('#contact').offset().top - headerHeight) {
        //
        // } else {
        //     // $('#contact').css({'zIndex': 1});
        // }
    });

    // fixes issue where clicking map link in safari will show slide 9
    $('#contact a').on('click', function() {
        var curPos = $(window).scrollTop();

        if(curPos >= $('#contact').offset().top - headerHeight - windowHeight) {
            $(slide).last().siblings().css('opacity', 0);
        }

    });



    $(slide).first().css('boxShadow', 'none');

    // Smooth scrolling to anchor
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - headerHeight
                }, 1000);
                return false;
            }
        }
    });

    $(window).on('scroll', function() {
      var curPos = $(this).scrollTop();

      $('.linkToSection').each(function() {
        var id = $(this).attr('id');
        var top = $(this).offset().top - headerHeight - 1;
        var bottom = top + $(this).outerHeight();


        if (curPos >= top && curPos <= bottom) {
          $('header a').removeClass('active');
          $('header a[href="#'+ id +'"]').addClass('active');

            if (history.pushState) {
                history.pushState(null, null, '#' + $(this).attr('id'));
            }
            else {
                location.hash = $(this).attr('id');
            }

        } else {
          $('header a[href="#'+ id +'"]').removeClass('active');
        }

      });
    });



}); // end document ready
